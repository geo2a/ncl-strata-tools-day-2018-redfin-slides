<!DOCTYPE html>
<html>
  <head>
    <title>Title</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

           @page{
        size: 6.27in 4.75in;
      }

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
      .title {font-size: 3.3em; font-weight:bold;letter-spacing:0.05em}
      .subtitle {font-size: 1.4em; color:#606060}
      .author {font-size: 1.4em; color:#606060;font-weight:bold;letter-spacing:0.02em}
      .coauthor {font-size: 1.0em; color:#606060;font-weight:bold;letter-spacing:0.02em}
      .institution {font-size: 1.0em;}
      .date {font-size: 1.0em;font-style: italic}

      blockquote {
          text-align: left;
          font: 28px italic Times, serif;
          padding: 8px;
          background-color: #faebbc;
          border-top: 1px solid #e1cc89;
          border-bottom: 1px solid #e1cc89;
          margin: 5px;
          background-image: url(img/openquote1.gif);
          background-position: top left;
          background-repeat: no-repeat;
          text-indent: 23px;
      }
      blockquote span {
          display: block;
          background-image: url(img/closequote1.gif);
          background-repeat: no-repeat;
          background-position: bottom right;
      }
      div.hardware-info {
        margin-top: -10px;
        font: 16px italic Times, serif;
      }

    </style>
  </head>
  <body>
    <textarea id="source">

name: Formal Methods for Space Electronics
class: center, middle

.title[{{name}}]

.subtitle[Formal Verification of Spacecraft Control Programs Using a Metalanguage for State Transformers]
<br>
<br>
<br>
.author[Georgy Lukyanov]
<br>
.institution[Newcastle University]
<br>
<br>
Supervisors:
.coauthor[Andrey Mokhov<sup>1</sup>, Alexander Romanovsky<sup>1</sup>, Jakob Lechner<sup>2</sup>]
<br>
.institution[<sup>1</sup>Newcastle University, <sup>2</sup>RUAG Space Austria]
<br>
<br>
<br>
.center[[{g.lukyanov2@newcastle.ac.uk}](g.lukyanov2@newcastle.ac.uk)]

.date[March 23, 2018]

---

name: Agenda

## {{name}}

1. Introduction
2. REDFIN instruction set architecture
3. REDFIN formal model
4. Verification example
5. Conclusion
---

name: Introduction
class: center, middle
.title[{{name}}]

---

.title[No Silver Bullet]
<br>
<br>
<br>
<blockquote><span>
  There is no single development, in either technology or management technique, which by itself promises even one order of magnitude improvement within a decade in productivity, in reliability, in simplicity.
  <footer>— <a href="http://www.cs.unc.edu/techreports/86-020.pdf">
    Turing Award winner Fred Brooks</a></footer>
</span></blockquote>

???

Verification activities, following engineering standards for space electronics,
typically outweigh programming and design tasks by a factor of two in terms of development hours

Formal verification doesn't substitute post-design testing and validation

Catch errors in the early development stages
---
class: center, middle
.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/rule.png">]

???

Exploit typed functional programming to build a EDSL for building programs

Include support for formal verification via an SMT solver

---
class: center, middle
.title[REDFIN ISA]

<footer>REDuced instruction set for Fixed-point & INteger arithmetic</footer>

???

Primary applications: simple control tasks, antenna pointing unit in satellites

Arithmetics

---

## REDFIN: minimalistic sequencer for space missions

### Goals

* Simple instruction set to achieve a small hardware footprint
* Reduced complexity to support formal verification of programs
* Deterministic behaviour for real-time applications

### Facts

* Configurable bit width for the data path, ranging from 8 to 64 bits
* 47 instructions
* 4 general purpose registers
* No caches, no pipelining, no speculative execution

???

Redfin ISA has been developed with formal verification in mind

The design is intentionally simple

The applications do not require high performance, but do require high assurance

---

name: Formal Model & Verification Framework
class: center, middle
.title[Formal Model </br>&</br> Verification Framework]

---

<!-- .title[Microarchitecture state and the state transformers] -->

**Definition 1 (informal).** Microarchitecture state

$$
S=\\{(r, m, ic, ir, p, f, c) : r \in R,
                               m \in M, ic \in A, ir \in I, p \in P, f \in F, c \in C\\}
$$

```haskell
data State = State { registers           :: RegisterBank
                   , memory              :: Memory
                   , instructionCounter  :: InstructionAddress
                   , instructionRegister :: InstructionCode
                   , program             :: Program
                   , flags               :: Flags
                   , clock               :: Clock }
```

**Definition 2.** State transformer

$$
T : S \rightarrow S
$$

A function mapping states to states.

```
data Redfin a = Redfin { transform :: State -> (a, State) }
```

---

.center[REDFIN formal model]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/overview.svg">]

???

Writing high-level Haskell code, writing low-level REDFIN assembly

Conventional simulation and code generation

State transformer is made explicit in the Haskell model thanks to Haskell's advanced type system

Verifying that the state transformer of a given program satisfies
certain properties by compiling its execution trace into an SMT formula

---

# Workflow options

* Conventional assembly programming: no formal verification
* Formally verified assembly programs
* Deriving assembly programs from Haskell
* Verifying Haskell programs
* The ultimate workflow: derive assembly from Haskell and verify equivalence

---

.center[Conventional workflow: non-verified assembly programming]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/path_to_assembly.svg">]

---

.center[Deriving assembly programs from Haskell]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/path_from_haskell_to_assembly.svg">]

---

.center[Verifying Haskell programs]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/path_haskell_to_smt.svg">]

---

.center[Verifying assembly programs]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/path_assembly_to_smt.svg">]

---

.center[Executing verified assembly programs]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/path_assembly_to_smt.svg">]

---

.center[Deriving assembly from Haskell and verifying equivalence]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/ultimate.svg">]

---

## REDFIN verification framework: features

* Implemented in Haskell
* Symbolic execution programs to produce SMT problems </br>(via SBV https://hackage.haskell.org/package/sbv)
* Verification of user-defined program properties via SMT solver (Z3)
---
count: false
## REDFIN verification framework: features

* Implemented in Haskell
* Symbolic execution programs to produce SMT problems </br>(via SBV https://hackage.haskell.org/package/sbv)
* Verification of user-defined program properties via SMT solver (Z3)

## Properties to verify

* Status of a certain flag (`Overflow`, `Halt`, `OutOfMemory` etc.)
* Threshold on the execution time (amount of system clock cycles)
* Correctness of the computed result
* Equivalence of programs (in terms of output)

---
class: center, middle
.title[Example]

???

Let me demonstrate the usage of the verification framework on an example

---

.center[Arithmetic verification: running Haskell on REDFIN ]

.center[<img style="width:85%;margin-top:-0.7em;margin-bottom:-0.8em"  src="./img/example_workflow.svg">]

???

Haskell function -> Typed Language of Expressions -> Assembly -> State transformer -> SMT formula -> Verification result

---

High-level Haskell expression

```
addHaskell :: Integral a => a -> a -> a
addHaskell x y = x + y
```

Compiling Haskell to REDFIN assembly

```
addHighLevel = do
    let x     = read $ IntegerVariable 0
        y     = read $ IntegerVariable 1
        temp  = Temporary 3
        stack = Stack 5
    compile r0 stack temp (addHaskell x y)
    halt
```

Compilation result (roughly)

```
addLowLevel = do
    let { x = 0; y = 1 }
    ld r0 x
    add r0 y
    halt
```
---

Checking for integer overflow

```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```

Executing the SMT solver...
```
ghci> proveWith z3 addNoOveflow
...
```
---
Checking for integer overflow

```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```

Executing the SMT solver... and **BOOM!**
```
ghci> proveWith z3 addNoOveflow
Falsifiable. Counter-example:
  t1 = 8748242276167214084 :: Int64
  t2 = 8646348300372410368 :: Int64
```
---
Checking for integer overflow

```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```

Executing the SMT solver... and **BOOM!**
```
ghci> proveWith z3 addNoOveflow
Elapsed time: 0.047s
Falsifiable. Counter-example:
  t1 = 8748242276167214084 :: Int64
  t2 = 8646348300372410368 :: Int64
```

```
ghci> (8748242276167214084 :: Int64) + (8646348300372410368 :: Int64)
-1052153497169927164
```
---
Checking for integer overflow</br>
Considering input constraints

```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
*   constrain $ x .>= 0 &&& x .<= 10 ^ 6
*   constrain $ y .>= 0 &&& y .<= 10 ^ 6
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```
---
Checking for integer overflow</br>
Considering input constraints

```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
*   constrain $ x .>= 0 &&& x .<= 10 ^ 6
*   constrain $ y .>= 0 &&& y .<= 10 ^ 6
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```

Executing the SMT solver...
```
ghci> proveWith z3 addNoOveflow
...
```
---
Checking for integer overflow</br>
Considering input constraints
```
addNoOverflow = do
    x <- forall "t1"
    y <- forall "t2"
*   constrain $ x .>= 0 &&& x .<= 10 ^ 6
*   constrain $ y .>= 0 &&& y .<= 10 ^ 6
    let mem = initialiseMemory [(0, x), (1, y), (3, 100)]
        finalState = simulate 100 $ boot addHighLevel mem
        result = readArray (registers finalState) 0
        overflow = readArray (flags finalState) (flagId Overflow)
    pure $ bnot overflow
```

Executing the SMT solver... All good.
```
ghci> proveWith z3 addNoOveflow
Elapsed time: 0.029s
Q.E.D.
```
---

## More properties to verify

```haskell
highLevelCorrect = do
    ...
    result = readArray (registers finalState) 0
    overflow = readArray (flags finalState) (flagId Overflow)
    pure $   bnot overflow
         &&& result .>= 0
```

```haskell
ghci> proveWith z3 highLevelCorrect
Q.E.D.
SMT clauses: 134
Z3 time elapsed: 4.804s
```

???

As I said before, we may verify the absence of integer overlow

We can verify several properties by conjuncting them together

---
class: center, middle

.title[Restrictions]
---

# Restrictions

* Symbolic termination problem
```
```
* NP-hard problems are still NP-hard

???

Unbounded loops are hard to deal with

If the halting of the program starts to depend on a symbolic value,
we are in trouble

SAT is NP complete, thats the very property of the problem to be exploited to solve
various computationally equivalent problems. However, factorising numbers still takes time.

---
class: center, middle

.title[Conclusion]
---
## REDFIN verification framework overview

* ~2000 LOC, Haskell
* High-level typed language compiled to REDFIN assembly
* Low-level Haskell-embedded assembly language
* Checking microarchitecture state properties
* Checking equivalence of programs

### Extra features

* Worst-case execution time analysis
* C-code generation for massive parallel testing (e.g. on a POETS machine)
---
count: false
## REDFIN verification framework overview

* ~2000 LOC, Haskell
* High-level typed language compiled to REDFIN assembly
* Low-level Haskell-embedded assembly language
* Checking microarchitecture state properties
* Checking equivalence of programs

### Extra features

* Worst-case execution time analysis
* C-code generation for massive parallel testing (e.g. on a POETS machine)

## Get in touch

Georgy Lukyanov
[{g.lukyanov2@newcastle.ac.uk}](g.lukyanov2@newcastle.ac.uk)

* Tuura website: https://tuura.org/
* Github for REDFIN source code (availible soon): https://github.com/tuura

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_HTML&delayStartupUntil=configured" type="text/javascript"></script>
    <script>
      var slideshow = remark.create({highlightLines: true,
                                     highlightLanguage: 'haskell'});
      // Setup MathJax
      MathJax.Hub.Config({
          tex2jax: {
            inlineMath: [ ['$','$'], ["\\(","\\)"] ],
            skipTags: ['script', 'noscript', 'style', 'textarea', 'pre']
          }
      });

      MathJax.Hub.Configured();
    </script>
  </body>
</html>